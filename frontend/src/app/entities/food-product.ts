import { levenshteinSimilarity } from "../utils";

export interface FoodProduct {
  name: string;
  priceUSD: number;
  calories: number;
}

export interface FoodProductIcon {
  name: string;
  icon: string;
}

export function getMatchScore(name1: string, name2: string): number {
  const words1 = name1.split(' ').map(w => w.trim()).filter(Boolean);
  const words2 = name2.split(' ').map(w => w.trim()).filter(Boolean);

  let bestScore = levenshteinSimilarity(name1, name2);

  for (const word1 of words1) {
    for (const word2 of words2) {
      if (word1.length <= 3 || word2.length <= 3) {
        continue;
      }

      let score = levenshteinSimilarity(word1, word2);
      if (score > bestScore) {
        bestScore = score;
      }
    }
  }

  return bestScore;
}

export function getFoodProductIcon(productName0: string, icons: FoodProductIcon[]): string {
  const productName = productName0.toLowerCase().trim();

  let bestScore = 0;
  let bestIcon = null;

  for (const icon of icons) {
    const name = icon.name.toLowerCase().trim();

    if (name === productName) {
      return icon.icon;
    }

    const score = getMatchScore(name, productName);

    if (score > bestScore) {
      bestIcon = icon.icon;
      bestScore = score;
    }
  }

  return bestIcon ?? '🔴';
}
