export interface FoodItem extends FoodItemPrototype {
  id: number;
}

export interface FoodItemPrototype {
  userId: number;
  takenDate: string;
  productName: string;
  productCalories: number;
  productPriceUSD: number | null;
}
