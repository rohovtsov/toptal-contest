export interface AccountDetails {
  id: number;
  firstName: string;
  lastName: string;
  avatarUrl: string;
}

export interface UserAccountDetails extends AccountDetails {
  dailyCaloriesLimit: number;
  monthlyBudgetLimitUSD: number;
}
