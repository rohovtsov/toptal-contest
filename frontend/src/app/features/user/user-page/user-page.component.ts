import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AuthAccountService } from "../../../shared/infrastructure/auth-account.service";
import { combineLatest, map, Observable } from "rxjs";
import { FoodItem, UserAccountDetails } from "../../../entities";
import { FoodItemsFacadeService } from "../../../shared/infrastructure/food-items-facade.service";

interface UserPageState {
  account: UserAccountDetails;
  food: FoodItem[];
}

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserPageComponent {
  state$: Observable<UserPageState>;

  constructor(
    readonly auth: AuthAccountService,
    readonly facade: FoodItemsFacadeService,
  ) {
    this.state$ = combineLatest([
      this.auth.getDetails<UserAccountDetails>(),
      this.facade.food(),
    ]).pipe(
      map(([account, food]) => {
        return { account, food };
      })
    );
  }
}
