import { ChangeDetectionStrategy, Component } from '@angular/core';
import {BehaviorSubject, combineLatest, map, Observable, scan} from "rxjs";
import {AuthAccountService} from "../../../shared/infrastructure/auth-account.service";
import {FoodItemsFacadeService} from "../../../shared/infrastructure/food-items-facade.service";
import {AccountDetails, FoodItem, UserAccountDetails} from "../../../entities";
import {UsersService} from "../../../shared/infrastructure/users.service";

interface AdminPageState {
  food: FoodItem[];
  users: UserAccountDetails[];
  selectedUser: UserAccountDetails | null;
  selectedUserFood: FoodItem[];
  selectedFood: FoodItem | null;
  account: AccountDetails;
}

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminPageComponent {
  state$: Observable<AdminPageState>;
  selectFood$ = new BehaviorSubject<FoodItem | null>(null);

  constructor(
    readonly auth: AuthAccountService,
    readonly users: UsersService,
    readonly facade: FoodItemsFacadeService,
  ) {
    const selectedFood$ = this.selectFood$.pipe(
      scan((acc, food) => {
        return !!acc && acc === food ? null : food;
      }, null as FoodItem | null)
    )

    this.state$ = combineLatest([
      this.auth.getDetails<AccountDetails>(),
      this.facade.food(),
      this.users.getUsers(),
      selectedFood$,
    ]).pipe(
      map(([account, food, users, selectedFood]) => {
        const selectedUser = users.find(u => u.id === selectedFood?.userId) ?? null;

        return {
          account,
          food,
          users,
          selectedFood,
          selectedUser,
          selectedUserFood: food?.filter(f => f.userId === selectedUser?.id) ?? [],
        };
      })
    );
  }
}
