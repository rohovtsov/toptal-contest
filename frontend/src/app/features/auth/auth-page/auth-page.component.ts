import {Component, OnDestroy, OnInit} from '@angular/core';
import { AuthAccountService } from "../../../shared/infrastructure/auth-account.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {from, map, Observable, switchMap} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DefinitionsService} from "../../../shared/infrastructure/definitions.service";

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  greetingIcon$: Observable<string>;

  constructor(
    readonly definitions: DefinitionsService,
    readonly snackbar: MatSnackBar,
    readonly auth: AuthAccountService,
    readonly router: Router,
  ) {
    this.form = new FormGroup({
      authToken: new FormControl(this.auth.getAuthToken() ?? '', Validators.required),
      authRole: new FormControl(this.auth.getAuthRole() ?? 'user', Validators.required),
    });
    this.greetingIcon$ = this.definitions.getFoodProductIcons().pipe(
      map((icons) => icons[Math.floor(Math.random() * icons.length)].icon),
    )
  }

  ngOnInit(): void {
    this.snackbar.dismiss();
  }

  onSubmit(): void {
    if (!this.form.valid) {
      return;
    }

    this.auth.login(this.form.value.authToken!.trim(), this.form.value.authRole!.trim()).pipe(
      switchMap(() => {
        const url = this.auth.getAuthRole() === 'user' ? '/user' : '/admin';
        return from(this.router.navigate([url]));
      })
    ).subscribe();
  }

  ngOnDestroy(): void { }
}
