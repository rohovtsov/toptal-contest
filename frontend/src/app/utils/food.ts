import {AccountDetails, FoodItem, FoodItemPrototype} from "../entities";
import {parseTimestampDate} from "./pretty-timestamp";

export function foodAction(item: FoodItemPrototype, action: string, users: AccountDetails[]) {
  let user = users.length > 1 ? users.find(u => u.id === item.userId) ?? null : null;

  if (user) {
    return `${action} «${item.productName}» for ${user.firstName}`;
  } else {
    return `${action} «${item.productName}»`;
  }
}

export function filterFoodSinceTime(food: FoodItem[], now: Date, days: number): FoodItem[] {
  const foods: FoodItem[] = [];

  for (let i = 0; i < days; i++) {
    foods.push(...filterFoodByDate(food, new Date(now.getTime() - 86400000 * i)));
  }

  return foods;
}

export function filterFoodByDate(food: FoodItem[], now: Date): FoodItem[] {
  const nowTime = now.getTime() / 1000;
  const parsedNow = parseTimestampDate(nowTime, nowTime);

  return food.filter((f) => {
    return parsedNow === parseTimestampDate(nowTime, new Date(f.takenDate).getTime() / 1000);
  })
}
