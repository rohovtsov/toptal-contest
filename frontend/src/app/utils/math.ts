export function precision(num: number, precision: number): number {
  const d = 10 ** precision;
  return Math.round(num * d) / d;
}
