import {catchError, Observable, shareReplay, take} from "rxjs";

export class RequestCache {
  private cache: Record<string, Observable<any>> = {};

  request<T>(request$: Observable<T>, key: string): Observable<T> {
    if (this.cache[key]) {
      return this.cache[key];
    }

    const sharedReq$ = request$.pipe(
      shareReplay(1),
      take(1)
    );

    this.cache[key] = sharedReq$.pipe(
      catchError((err) => {
        delete this.cache[key];
        throw err;
      })
    );

    return sharedReq$;
  }

  clear(): void {
    this.cache = {};
  }
}
