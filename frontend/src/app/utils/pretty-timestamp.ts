export const parseTimestamp = (timestamp0: number, now0: number): string => {
  const now = Math.floor(now0 / 1000);
  const timestamp = Math.floor(timestamp0 / 1000);

  return parseTimestampTime(timestamp, now) + ', ' + parseTimestampDate(timestamp, now);
}

export const parseTimestampTime = (timestamp: number, now: number): string => {
  const past = (now - timestamp);

  if (past < 10) {
    return 'just now';
  } else if (past < 60) {
    const num = Math.floor(past / 5) * 5;
    return num + ' sec ago';
  } else if (past < 3600) {
    const minutes = Math.round(past / 60);
    return minutes + ' min ago';
  } else if (past < 3600 * 12) {
    const hours = Math.round(past / 3600);
    return hours + ' hrs ago';
  } else {
    const date = new Date(timestamp * 1000);
    const minutes = date.getMinutes();
    return date.getHours() + ':' + (minutes < 10 ? '0' : '') + minutes;
  }
};

export const parseTimestampDate = (timestamp: number, now: number): string => {
  const date = new Date(timestamp * 1000);
  const nowDate = new Date(now * 1000);
  const yesterdayDate = new Date(now * 1000 - 86400000);

  const isToday = nowDate.getDate() === date.getDate() && nowDate.getMonth() === date.getMonth() && nowDate.getFullYear() === date.getFullYear();
  const isYesterday = yesterdayDate.getDate() === date.getDate() && yesterdayDate.getMonth() === date.getMonth() && yesterdayDate.getFullYear() === date.getFullYear();

  if (isToday) {
    return 'today';
  } else if (isYesterday) {
    return 'yesterday';
  } else {
    return date.getDate() + ' ' + MONTH_ABBRS[date.getMonth()] + ' ' + date.getFullYear();
  }
};

export const MONTH_ABBRS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
