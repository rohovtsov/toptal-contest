import { Injectable } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {FoodItem, FoodItemPrototype} from "../../entities";
import {BehaviorSubject, catchError, concatMap, EMPTY, forkJoin, Observable, of, switchMap} from "rxjs";
import {FoodEditDialogComponent} from "../components/food-edit-dialog/food-edit-dialog.component";
import {AuthAccountService} from "./auth-account.service";
import {UsersService} from "./users.service";
import {FoodItemsService} from "./food-items.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DefinitionsService} from "./definitions.service";
import {foodAction} from "../../utils";

@Injectable({
  providedIn: 'root'
})
export class FoodItemsFacadeService {
  changed$ = new BehaviorSubject<number>(0);

  constructor(
    private matDialog: MatDialog,
    private auth: AuthAccountService,
    private foodItems: FoodItemsService,
    private definitions: DefinitionsService,
    private users: UsersService,
    private snackbar: MatSnackBar,
  ) { }

  food(): Observable<FoodItem[]> {
    return this.changed$.pipe(
      switchMap(() => this.foodItems.getFood()),
    );
  }

  add(): Observable<void> {
    return this.createPrototype().pipe(
      concatMap((prototype) => this.modifyFoodItems(
        this.foodItems.addFood(prototype),
        prototype,
        'Added'
      ))
    );
  }

  edit(foodItem: FoodItem): Observable<void> {
    return this.createPrototype(foodItem).pipe(
      concatMap((prototype) => this.modifyFoodItems(
        this.foodItems.editFood(foodItem.id, prototype),
        prototype,
        'Edited'
      ))
    );
  }

  remove(foodItem: FoodItem): Observable<void> {
    return this.modifyFoodItems(
      this.foodItems.removeFood(foodItem.id),
      foodItem,
      'Deleted'
    );
  }

  private modifyFoodItems(modification$: Observable<true>, prototype: FoodItemPrototype, action: string): Observable<void> {
    return modification$.pipe(
      switchMap(() => {
        this.changed$.next(this.changed$.value + 1);
        return this.users.getUsers().pipe(
          switchMap((users) => {
            this.snackbarMessage(foodAction(prototype, action, users));
            return EMPTY;
          })
        );
      }),
      catchError((err) => {
        this.snackbarError(err);
        return EMPTY;
      }),
    )
  }

  private createPrototype(foodItem?: FoodItem): Observable<FoodItemPrototype> {
    return forkJoin([
      this.users.getUsers(),
      this.definitions.getFoodProducts(),
    ]).pipe(
      catchError((err) => {
        this.snackbarError(err);
        return EMPTY;
      }),
      switchMap(([users, products]) => this.matDialog.open(FoodEditDialogComponent, {
        data: { foodItem, products, users },
        autoFocus: false,
      }).afterClosed().pipe(
        switchMap(proto => {
          if (!proto) {
            return EMPTY;
          } else {
            return of(proto);
          }
        }),
      ))
    ) as Observable<FoodItemPrototype>;
  }

  private snackbarMessage(message: string): void {
    this.snackbar.open(message, undefined, {
      duration: 4000,
      horizontalPosition: 'start',
      verticalPosition: 'bottom'
    });
  }

  private snackbarError(err: any): void {
    const message = err?.error?.message ?? err?.message ?? 'Unknown error';
    this.snackbarMessage(message);
  }
}
