import { Injectable } from '@angular/core';
import {AuthAccountService} from "./auth-account.service";

@Injectable({
  providedIn: 'root'
})
export class GreetingsService {
  private greetings = [
    'Welcome,',
    'Hello,',
    'Hola,',
    'Great to see you,',
    'What’s up?',
  ];
  private prevGreetings: string[] = [];

  constructor(
    private auth: AuthAccountService,
  ) { }

  public getGreeting(): string {
    return this.isAdminMode() ? 'Admin mode' : this.getGreetingForUser();
  }

  public isAdminMode(): boolean {
    return this.auth.getAuthRole() === 'admin';
  }

  private getGreetingForUser(): string {
    const maxPrevGreets = Math.min(Math.ceil(this.greetings.length / 2), this.greetings.length - 1);

    let greetingId;
    do {
      greetingId = Math.floor(Math.random() * this.greetings.length);
    } while (this.prevGreetings.includes(this.greetings[greetingId]));

    const greeting = this.greetings[greetingId];
    this.prevGreetings.push(greeting);

    while (this.prevGreetings.length > maxPrevGreets) {
      this.prevGreetings.splice(0, 1);
    }

    return greeting;
  }
}
