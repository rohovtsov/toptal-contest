import {Inject, Injectable} from '@angular/core';
import {AccountDetails, API_URL} from "../../entities";
import {HttpClient} from "@angular/common/http";
import {EMPTY, Observable, switchMap} from "rxjs";
import {RequestCache} from "../../utils";

export type AccountRole = 'user' | 'admin';

@Injectable({
  providedIn: 'root'
})
export class AuthAccountService {
  private authToken: string | null;
  private authRole: AccountRole;
  private cache = new RequestCache();

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private apiUrl: string
  ) {
    this.authRole = window.localStorage.getItem('auth-role') as AccountRole ?? 'user';
    this.authToken = window.localStorage.getItem('auth-token') as AccountRole ?? null;
  }

  logout(): Observable<void> {
    this.cache.clear();
    this.setAuth(null, 'user');
    return this.requestDetails(true).pipe(
      switchMap(() => EMPTY)
    );
  }

  login(authToken: string, authRole: string | AccountRole): Observable<AccountDetails> {
    this.cache.clear();
    this.setAuth(authToken, authRole as AccountRole);
    return this.requestDetails(false);
  }

  getAuthToken(): string | null {
    return this.authToken;
  }

  getAuthRole(): AccountRole | null {
    return this.authRole;
  }

  getDetails<T extends AccountDetails>(): Observable<T> {
    return this.cache.request(
      this.requestDetails(),
      `${this.authRole}/details`,
    );
  }

  private setAuth(token: string | null, role: AccountRole): void {
    this.authToken = token;
    this.authRole = role;
    window.localStorage.setItem('auth-role', this.authRole);

    if (this.authToken) {
      window.localStorage.setItem('auth-token', this.authToken);
    } else {
      window.localStorage.removeItem('auth-token');
    }
  }

  private requestDetails<T extends AccountDetails>(quiet = false): Observable<T> {
    return this.http.get<T>(`${this.apiUrl}/${this.authRole}/details`, {
      headers: quiet ? { 'interceptor': 'quiet' } : {}
    });
  }
}
