import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {DefinitionsService} from "./definitions.service";
import {AuthAccountService} from "./auth-account.service";
import {FoodItemsService} from "./food-items.service";
import {GreetingsService} from "./greetings.service";
import {UsersService} from "./users.service";
import {FoodItemsFacadeService} from "./food-items-facade.service";
import {AuthAccountInterceptor} from "./auth-account.interceptor";



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MatDialogModule,
    MatSnackBarModule,
  ],
  providers: [
    DefinitionsService,
    AuthAccountService,
    FoodItemsService,
    GreetingsService,
    UsersService,
    FoodItemsFacadeService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthAccountInterceptor, multi: true },
  ],
})
export class SharedInfrastructureModule { }
