import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {API_URL, UserAccountDetails} from "../../entities";
import {map, Observable, of} from "rxjs";
import {AuthAccountService} from "./auth-account.service";
import {RequestCache} from "../../utils";

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private cache = new RequestCache();

  constructor(
    private http: HttpClient,
    private auth: AuthAccountService,
    @Inject(API_URL) private apiUrl: string
  ) { }

  getUsers(): Observable<UserAccountDetails[]> {
    if (this.auth.getAuthRole() === 'admin') {
      return this.cache.request<UserAccountDetails[]>(
        this.http.get<UserAccountDetails[]>(`${this.apiUrl}/admin/users`),
        '/admin/users'
      );
    } else {
      return this.auth.getDetails().pipe(
        map((user) => ([user as UserAccountDetails]))
      );
    }
  }
}
