import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {catchError, from, Observable} from 'rxjs';
import { AuthAccountService } from "./auth-account.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class AuthAccountInterceptor implements HttpInterceptor {
  constructor(
    private auth: AuthAccountService,
    private router: Router,
    private snackbar: MatSnackBar,
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authToken = this.auth.getAuthToken();
    let isQuiet = false;
    let modifiedRequest = request;

    if (authToken) {
      modifiedRequest = modifiedRequest.clone({
        headers: modifiedRequest.headers.set('Authorization', authToken)
      });
    }

    if (modifiedRequest.headers.get('interceptor') === 'quiet') {
      modifiedRequest = modifiedRequest.clone({
        headers: modifiedRequest.headers.delete('interceptor')
      });
      isQuiet = true;
    }

    return next.handle(modifiedRequest).pipe(
      catchError(err => {
        const isForbidden = err?.error?.statusCode === 403;

        if (isForbidden && !isQuiet) {
          this.snackbar.open(err.error.message, undefined, { duration: 4000, horizontalPosition: 'start', verticalPosition: 'bottom' });
        }

        if (isForbidden) {
          this.router.navigate(['']);
        }
        throw err;
      })
    );
  }
}
