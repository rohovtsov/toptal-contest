import {Inject, Injectable} from '@angular/core';
import {API_URL, FoodProduct, FoodProductIcon} from "../../entities";
import {HttpClient} from "@angular/common/http";
import {EMPTY, forkJoin, Observable, switchMap} from "rxjs";
import {RequestCache} from "../../utils";

@Injectable({
  providedIn: 'root'
})
export class DefinitionsService {
  private cache = new RequestCache();

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private apiUrl: string
  ) { }

  preload(): Observable<void> {
    return forkJoin([
      this.getFoodProductIcons(),
      this.getFoodProducts(),
    ]).pipe(
      switchMap(() => EMPTY)
    )
  }

  getFoodProducts(): Observable<FoodProduct[]> {
    return this.cache.request(
      this.http.get<FoodProduct[]>(`${this.apiUrl}/definitions/products`),
      '/definitions/products'
    );
  }

  getFoodProductIcons(): Observable<FoodProductIcon[]> {
    return this.cache.request(
      this.http.get<FoodProductIcon[]>(`${this.apiUrl}/definitions/productIcons`),
      '/definitions/productIcons'
    );
  }
}
