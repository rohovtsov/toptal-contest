import {Inject, Injectable} from '@angular/core';
import {AuthAccountService} from "./auth-account.service";
import {HttpClient} from "@angular/common/http";
import {API_URL, FoodItem, FoodItemPrototype} from "../../entities";
import { map, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FoodItemsService {
  constructor(
    private auth: AuthAccountService,
    private http: HttpClient,
    @Inject(API_URL) private apiUrl: string
  ) { }

  getFood(): Observable<FoodItem[]> {
    return this.http.get<FoodItem[]>(`${this.apiUrl}/${this.auth.getAuthRole()}/food`);
  }

  editFood(foodId: number, prototype: FoodItemPrototype): Observable<true> {
    return this.http.put(`${this.apiUrl}/${this.auth.getAuthRole()}/food/${foodId}`, prototype, {
      params: { userId: prototype.userId }
    }).pipe(
      map(() => true)
    );
  }

  addFood(prototype: FoodItemPrototype): Observable<true> {
    return this.http.post(`${this.apiUrl}/${this.auth.getAuthRole()}/food`, prototype, {
      params: { userId: prototype.userId }
    }).pipe(
      map(() => true)
    );
  }

  removeFood(foodId: number): Observable<true> {
    return this.http.delete(`${this.apiUrl}/${this.auth.getAuthRole()}/food/${foodId}`).pipe(
      map(() => true)
    );
  }
}
