import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {UserAccountDetails} from "../../../entities";

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserViewComponent implements OnInit {
  @Input() user?: UserAccountDetails;

  constructor() { }

  ngOnInit(): void {
  }

}
