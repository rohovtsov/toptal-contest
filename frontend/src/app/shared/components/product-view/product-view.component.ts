import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit} from '@angular/core';
import {DefinitionsService} from "../../infrastructure/definitions.service";
import {map, Observable} from "rxjs";
import {getFoodProductIcon} from "../../../entities";

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductViewComponent implements OnChanges {
  @Input() productName?: string;
  icon$?: Observable<string>;

  constructor(
    private definitions: DefinitionsService
  ) { }

  ngOnChanges(): void {
    if (this.productName) {
      this.icon$ = this.definitions.getFoodProductIcons().pipe(
        map((icons) => getFoodProductIcon(this.productName!, icons))
      )
    }
  }
}
