import { Pipe, PipeTransform} from '@angular/core';
import { parseTimestamp } from '../../utils';
import { interval } from "rxjs";

@Pipe({
  name: 'prettyDate',
  pure: false,
})
export class PrettyDatePipe implements PipeTransform {
  transform(value: string | number): string {
    const now = Date.now();
    const time = new Date(value).getTime();
    return parseTimestamp(time, now);
  }
}

export function prettyDateSyncer() {
  return interval(5000);
}
