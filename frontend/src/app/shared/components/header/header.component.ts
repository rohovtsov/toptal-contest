import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccountDetails} from "../../../entities";
import {GreetingsService} from "../../infrastructure/greetings.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
  @Input() account: AccountDetails | null = null;
  @Output() logout = new EventEmitter<void>();
  @Output() newFood = new EventEmitter<void>();
  greeting: string;
  isAdmin: boolean;

  constructor(
    private greetingsService: GreetingsService
  ) {
    this.greeting = this.greetingsService.getGreeting();
    this.isAdmin = this.greetingsService.isAdminMode();
  }

  ngOnInit(): void {
  }
}
