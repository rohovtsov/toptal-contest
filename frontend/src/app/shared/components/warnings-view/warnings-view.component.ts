import {ChangeDetectionStrategy, Component, Input, OnChanges} from '@angular/core';
import {FoodItem, UserAccountDetails} from "../../../entities";
import {filterFoodByDate, filterFoodSinceTime, precision} from "../../../utils";

@Component({
  selector: 'app-warnings-view',
  templateUrl: './warnings-view.component.html',
  styleUrls: ['./warnings-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WarningsViewComponent implements OnChanges {
  @Input() user?: UserAccountDetails;
  @Input() food: FoodItem[] = [];
  @Input() extended = false;
  caloriesLimit = 0;
  budgetLimit = 0;
  caloriesUsed = 0;
  budgetUsed = 0;
  entriesThisWeek = 0;
  entriesPastWeek = 0;
  averageCalories = 0;

  ngOnChanges(): void {
    this.caloriesLimit = this.user?.dailyCaloriesLimit ?? 0;
    this.budgetLimit = this.user?.monthlyBudgetLimitUSD ?? 0;

    const foodForCalories = filterFoodByDate(this.food, new Date());
    const foodForBudget = filterFoodSinceTime(this.food, new Date(), 30);

    this.caloriesUsed = precision(foodForCalories.reduce((acc, food) => {
      return acc + food.productCalories;
    }, 0), 0);

    this.budgetUsed = precision(foodForBudget.reduce((acc, food) => {
      return acc + (food.productPriceUSD ?? 0);
    }, 0), 2);

    const thisWeekFood = filterFoodSinceTime(this.food, new Date(), 7);

    this.averageCalories = precision(thisWeekFood.reduce((acc, food) => {
      return acc + food.productCalories;
    }, 0) / 7, 2);
    this.entriesThisWeek = thisWeekFood.length;
    this.entriesPastWeek = filterFoodSinceTime(this.food, new Date(new Date().getTime() - 7 * 86400001), 7).length;
  }
}
