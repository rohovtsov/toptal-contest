import {FoodItem} from "../../../entities";
import {parseTimestampDate} from "../../../utils";

export interface Bar {
  label: string;
  progress: number;
  exceed: number;
  count: number;
}

export const createBars = (
  food: FoodItem[],
  count: number,
  startFromTimestamp: number,
  caloriesLimit: number,
): Bar[] => {
  const foodByDate = food.reduce((acc, fI) => {
    const date = new Date(fI.takenDate);
    const dateKey = dateToStrKey(date);

    acc[dateKey] = [
      ...(acc[dateKey] ?? []),
      fI
    ];

    return acc;
  }, {} as Record<string, FoodItem[]>);

  const rawBars = Array.from({ length: count }).map((val, i) => {
    const date = new Date((startFromTimestamp - 86400 * i) * 1000);
    const dateKey = dateToStrKey(date);
    const foodItems = foodByDate[dateKey] ?? [];

    const calories = foodItems.filter(f => f.productCalories > 0)
      .reduce((acc, f) => acc + f.productCalories, 0);

    const exceed = Math.max(0, calories - caloriesLimit);

    return <Bar>{
      label: parseTimestampDate(Math.round(date.getTime() / 1000), startFromTimestamp),
      progress: 0,
      exceed: exceed / calories,
      count: calories
    };
  });

  const maxCount = rawBars.reduce((acc, bar) => Math.max(acc, bar.count), 0);

  const bars = rawBars.map((bar) => {
    return {
      ...bar,
      progress: bar.count / maxCount
    };
  }).reverse();

  return bars;
};

export const setBarsPrecision = (bars: Bar[], precision: number): Bar[] => {
  return bars.map((bar) => {
    const num = 10 ** precision;
    return {
      ...bar,
      count: Math.round(bar.count * num) / num
    };
  });
};

const dateToStrKey = (date: Date): string => {
  return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear();
};
