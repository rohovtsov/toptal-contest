import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges
} from '@angular/core';
import {Bar, createBars, setBarsPrecision} from './bars';
import {FoodItem, UserAccountDetails} from "../../../entities";

@Component({
  selector: 'app-statistics-view',
  templateUrl: './statistics-view.component.html',
  styleUrls: ['./statistics-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatisticsViewComponent implements OnChanges {
  @Input() user?: UserAccountDetails;
  @Input() food: FoodItem[] = [];
  loadedBarsHash: string | null = null;
  isLoaded = false;
  bars: Bar[] = [];
  total = 0;

  constructor(
    private ch: ChangeDetectorRef
  ) { }

  ngOnChanges(): void {
    const limit = this.user?.dailyCaloriesLimit ?? 0;
    this.bars = setBarsPrecision(createBars(this.food, 7, Math.round(Date.now() / 1000), limit), 2);
    this.total = Math.round(this.bars.reduce((acc, bar) => acc + bar.count, 0) * 100) / 100;

    const hash = JSON.stringify(this.bars);
    if (hash !== this.loadedBarsHash) {
      this.loadedBarsHash = hash;
      this.playReloadAnimation();
    }
  }

  private playReloadAnimation(): void {
    this.isLoaded = false;

    setTimeout(() => {
      this.isLoaded = true;
      this.ch.detectChanges();
    }, 0);
  }

  public getCountLabel(count: number): string {
    return count + ' C.';
  }

  barsTrackBy(index: number, bar: Bar): number {
    return index;
  }
}
