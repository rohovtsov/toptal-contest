import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { HeaderComponent } from './header/header.component';
import {MatButtonModule} from "@angular/material/button";
import { FoodTableComponent } from './food-table/food-table.component';
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import { UserViewComponent } from './user-view/user-view.component';
import { FindUserPipe } from './find-user.pipe';
import {ProductViewComponent} from "./product-view/product-view.component";
import {PrettyDatePipe} from "./pretty-date.pipe";
import {MatTooltipModule} from "@angular/material/tooltip";
import { WarningsViewComponent } from './warnings-view/warnings-view.component';
import {StatisticsViewComponent} from "./statistics-view/statistics-view.component";
import { FoodEditDialogComponent } from './food-edit-dialog/food-edit-dialog.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatSnackBarModule} from "@angular/material/snack-bar";



@NgModule({
  declarations: [
    FindUserPipe,
    PrettyDatePipe,
    HeaderComponent,
    FoodTableComponent,
    UserViewComponent,
    ProductViewComponent,
    WarningsViewComponent,
    StatisticsViewComponent,
    FoodEditDialogComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatSnackBarModule,
  ],
  exports: [
    HeaderComponent,
    FoodTableComponent,
    ProductViewComponent,
    UserViewComponent,
    FindUserPipe,
    PrettyDatePipe,
    WarningsViewComponent,
    StatisticsViewComponent,
    FoodEditDialogComponent,
  ]
})
export class SharedComponentsModule { }
