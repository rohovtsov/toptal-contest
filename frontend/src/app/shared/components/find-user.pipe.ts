import { Pipe, PipeTransform } from '@angular/core';
import { AccountDetails } from "../../entities";

@Pipe({
  name: 'findUser',
})
export class FindUserPipe implements PipeTransform {
  transform<T extends AccountDetails>(value: T[], userId: number): T | null {
    return value.find((u) => u.id === userId) ?? null;
  }
}
