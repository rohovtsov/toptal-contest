import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit, Output
} from '@angular/core';
import {AccountDetails, FoodItem} from "../../../entities";
import {map, Observable, of, startWith, Subscription} from "rxjs";
import {prettyDateSyncer} from "../pretty-date.pipe";
import {foodAction} from "../../../utils";
import {FormControl, FormGroup} from "@angular/forms";

const COLUMNS = ['userId', 'productName', 'productCalories', 'productPriceUSD', 'takenDate', 'actions'];

@Component({
  selector: 'app-food-table',
  templateUrl: './food-table.component.html',
  styleUrls: ['./food-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FoodTableComponent implements OnChanges, OnInit, OnDestroy {
  @Input() food: FoodItem[] = [];
  @Input() users: AccountDetails[] = [];
  @Input() displayUser = true;
  @Input() selected: FoodItem | null = null;
  @Output() selectFood = new EventEmitter<FoodItem>();
  @Output() removeFood = new EventEmitter<FoodItem>();
  @Output() editFood = new EventEmitter<FoodItem>();
  columns: string[] = [];
  subscription?: Subscription;
  today = new Date();
  filterForm = new FormGroup({
    since: new FormControl(new Date(this.today.getTime() - 86400000 * 7)),
    till: new FormControl(null),
  });
  filteredFood$: Observable<FoodItem[]> = of([]);

  constructor(
    private ch: ChangeDetectorRef,
  ) { }

  ngOnChanges(): void {
    if (this.displayUser) {
      this.columns = COLUMNS;
    } else {
      this.columns = COLUMNS.filter((col) => col !== 'userId');
    }

    this.filteredFood$ = this.filterFood$(this.food);
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription = prettyDateSyncer().subscribe(() => {
      this.ch.detectChanges();
    })
  }

  actionName(action: string, element: FoodItem): string {
    return foodAction(element, action, this.users);
  }

  private filterFood$(food: FoodItem[]): Observable<FoodItem[]> {
    return this.filterForm.valueChanges.pipe(
      startWith(this.filterForm.value as { since: Date | null, till: Date | null }),
      map((filter) => {
        const from: number | null = filter.since ? new Date(filter.since.getFullYear(), filter.since.getMonth(), filter.since.getDate()).getTime() : null;
        const toDate = filter.till ? new Date(filter.till.getFullYear(), filter.till.getMonth(), filter.till.getDate()) : null;
        const to: number | null = toDate ? new Date(toDate.getTime() + 86400000).getTime() : null;

        return food.filter((f) => {
          const time = new Date(f.takenDate).getTime();
          return (
            (from === null || time >= from) &&
            (to === null || time <= to)
          );
        })
      })
    )
  }
}
