import { ChangeDetectionStrategy, Component, Inject, OnInit, Optional } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FoodItem, FoodItemPrototype, FoodProduct, UserAccountDetails} from "../../../entities";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {distinctUntilChanged, EMPTY, map, Observable, startWith} from "rxjs";
import {MatOptionSelectionChange} from "@angular/material/core";
import {precision} from "../../../utils";

export interface FoodEditDialogData {
  foodItem?: FoodItem;
  users: UserAccountDetails[];
  products: FoodProduct[];
}

@Component({
  selector: 'app-food-edit-dialog',
  templateUrl: './food-edit-dialog.component.html',
  styleUrls: ['./food-edit-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FoodEditDialogComponent implements OnInit {
  isEdit = false;
  foodItem?: FoodItem;
  users: UserAccountDetails[] = [];
  products: FoodProduct[] = [];
  filteredProducts$: Observable<FoodProduct[]> = EMPTY;
  today = new Date();
  form: FormGroup;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: FoodEditDialogData,
    readonly dialogRef: MatDialogRef<FoodEditDialogComponent>,
  ) {
    this.form = createForm();
  }

  ngOnInit(): void {
    this.foodItem = this.data?.foodItem;
    this.users = this.data?.users ?? [];
    this.products = this.data?.products ?? [];
    this.isEdit = !!this.foodItem;

    const withUserId = !this.isEdit && this.users.length > 1;
    this.form = createForm(this.foodItem, withUserId);
    const productNameControl = this.form.get('productName')!;

    this.filteredProducts$ = productNameControl.valueChanges.pipe(
      startWith(productNameControl.value),
      distinctUntilChanged(),
      map((value0) => {
        const value = (value0 || '').toLowerCase().trim();

        return this.products.filter(product => {
          return product.name.toLowerCase().trim().includes(value);
        });
      }),
    );
  }

  onProductSelect(event: MatOptionSelectionChange<string>) {
    const product = this.products.find(p => p.name === event.source.value);
    if (!event.source.selected || !product) {
      return;
    }

    this.form.patchValue({
      productName: product.name,
      productCalories: product.calories,
      productPriceUSD: product.priceUSD,
    });
  }

  onSubmit(): void {
    if (!this.form.valid) {
      return;
    }

    this.dialogRef.close(createPrototype(this.users, this.foodItem, this.form.value));
  }
}

function createForm(foodItem?: FoodItem, withUserId = false): FormGroup {
  let date = new Date().toISOString();
  let time = currentTime();

  if (foodItem?.takenDate) {
    const date = new Date(foodItem!.takenDate);
    time = `${date.getHours()}:${date.getMinutes()}`;
  }

  if (foodItem?.takenDate) {
    date = foodItem?.takenDate;
  }

  const dateControl = new FormControl(date, Validators.required);
  const controls: Record<string, AbstractControl> = {
    takenDate: dateControl,
    takenTime: new FormControl(time, [
      Validators.required,
      (control) => {
        const time = validateTime(control.value);

        if (!time) {
          return { invalidTime: true };
        }

        if (dateControl.value && new Date(composeDate(dateControl.value, control.value)).getTime() >= new Date().getTime()) {
          return { timeInFuture: true };
        }

        return null;
      }
    ]),
    productName: new FormControl(foodItem?.productName ?? '', Validators.required),
    productCalories: new FormControl(foodItem?.productCalories ?? '', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.min(0.01),
    ]),
    productPriceUSD: new FormControl(foodItem?.productPriceUSD ?? '', [
      Validators.pattern("([0-9]*[.])?[0-9]+"),
      Validators.min(0.01),
    ]),
  };

  if (withUserId) {
    controls['userId'] = new FormControl(foodItem?.userId ?? null, Validators.required);
  }

  return new FormGroup(controls);
}

function composeDate(date: string, time: string): string {
  const d = new Date(date);
  const a = validateTime(time)

  return new Date(d.getFullYear(), d.getMonth(), d.getDate(), a?.[0], a?.[1]).toISOString();
}

function validateTime(time: string): [number, number] | null {
  const h = Number(time.split(':')?.[0]?.trim());
  const m = Number(time.split(':')?.[1]?.trim());
  const test = new Date(0, 0, 0, h, m);

  if (h === test.getHours() && m === test.getMinutes()) {
    return [h, m];
  }

  return null;
}

function createPrototype(users: UserAccountDetails[], foodItem?: FoodItem, value?: any): FoodItemPrototype {
  return {
    userId: Number(foodItem?.userId ?? value?.userId ?? users?.[0] ?? 0),
    productName: String(value?.productName ?? ''),
    productCalories: Number(value?.productCalories ?? 0),
    productPriceUSD: value?.productPriceUSD ? precision(Number(value.productPriceUSD!), 2) : null,
    takenDate: composeDate(value.takenDate, value.takenTime),
  };
}

function currentTime(): string {
  const h = new Date().getHours();
  const m = new Date().getMinutes();
  return `${h}:${m < 10 ? '0' : ''}${m}`;
}
