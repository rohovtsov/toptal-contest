import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { API_URL } from './entities';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DefinitionsService} from "./shared/infrastructure/definitions.service";
import {appInitializer} from "./app.initializer";
import {MatIconRegistry} from "@angular/material/icon";
import {SharedInfrastructureModule} from "./shared/infrastructure/shared-infrastructure.module";
import {SharedComponentsModule} from "./shared/components/shared-components.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedComponentsModule,
    SharedInfrastructureModule,
  ],
  providers: [
    { provide: API_URL, useValue: environment.apiUrl },
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      deps: [DefinitionsService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry) {
    matIconRegistry.setDefaultFontSetClass('material-icons-outlined');
  }
}
