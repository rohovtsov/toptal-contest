import { DefinitionsService } from "./shared/infrastructure/definitions.service";

export function appInitializer(definitions: DefinitionsService) {
  return () => {
    return definitions.preload();
  };
}
