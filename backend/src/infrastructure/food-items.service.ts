import { Injectable } from '@nestjs/common';
import { FoodItem, FoodItemPrototype } from '../entities';

@Injectable()
export class FoodItemsService {
  food: FoodItem[];

  constructor() {
    this.food = [];
  }

  getFood(
    userId: number | null = null,
    fromDate: string | null = null,
    toDate: string | null = null,
  ): FoodItem[] {
    let food = this.food;

    if (userId !== null) {
      food = food.filter((food) => food.userId === userId);
    }

    if (fromDate !== null) {
      food = food.filter(
        (food) =>
          new Date(food.takenDate).getTime() >= new Date(fromDate).getTime(),
      );
    }

    if (toDate !== null) {
      food = food.filter(
        (food) =>
          new Date(food.takenDate).getTime() <= new Date(toDate).getTime(),
      );
    }

    return food.sort(
      (a, b) =>
        new Date(b.takenDate).getTime() - new Date(a.takenDate).getTime(),
    );
  }

  removeFood(food: FoodItem): void {
    const index = this.food.indexOf(food);

    if (index === -1) {
      throw new Error('Food not found');
    }

    this.food.splice(index, 1);
  }

  updateFood(food: FoodItem, newFood: FoodItemPrototype): void {
    const index = this.food.indexOf(food);

    if (index === -1) {
      throw new Error('Food not found');
    }

    if (newFood.productPriceUSD !== undefined) {
      food.productPriceUSD = newFood.productPriceUSD;
    }

    food.productName = newFood.productName;
    food.productCalories = newFood.productCalories;
    food.takenDate = newFood.takenDate;
  }

  addFood(userId: number, newFood: FoodItemPrototype): void {
    this.food.push({
      id: this.nextFoodId(),
      userId,
      ...newFood,
    });
  }

  getFoodById(id: number, userId: number | null = null): FoodItem | null {
    const food = this.food.find((f) => f.id === id);

    if (!food || (userId !== null && food.userId !== userId)) {
      return null;
    }

    return food;
  }

  private nextFoodId(): number {
    let nextId;

    do {
      nextId = Math.round(Math.random() * 1000000000 + 1);
    } while (!!this.getFoodById(nextId));

    return nextId;
  }
}
