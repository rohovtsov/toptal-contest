import { Injectable } from '@nestjs/common';
import {
  AccountDetails,
  AccountFullDetails,
  AccountRole,
  UserAccountDetails,
} from '../entities';
import * as fs from 'fs';

@Injectable()
export class AccountsService {
  accounts: AccountFullDetails[] = [];

  constructor() {
    this.accounts = JSON.parse(
      fs.readFileSync('./assets/accounts.json').toString(),
    ).accounts;
  }

  hasUserAccount(userId: number): boolean {
    return !!this.accounts.find((u) => u.id === userId);
  }

  getUserAccounts(): UserAccountDetails[] {
    return this.accounts
      .filter((account) => account.role === 'user')
      .map(AccountsService.safeAccount<UserAccountDetails>.bind(this));
  }

  getAccountByToken<T extends AccountDetails>(
    token: string,
    role: AccountRole,
  ): T | null {
    const account = this.accounts.find(
      (account) => account.role === role && account.authToken === token,
    );

    return account ? AccountsService.safeAccount<T>(account) : null;
  }

  private static safeAccount<T extends AccountDetails>(
    account: AccountFullDetails,
  ): T {
    const safe = { ...account };
    delete safe.authToken;
    delete safe.role;
    return safe as unknown as T;
  }
}
