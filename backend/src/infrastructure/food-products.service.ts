import { Injectable } from '@nestjs/common';
import { FoodProductIcon, FoodProduct } from '../entities';
import * as fs from 'fs';

@Injectable()
export class FoodProductsService {
  private readonly products: FoodProduct[] = [];
  private readonly icons: FoodProductIcon[] = [];

  constructor() {
    this.products = JSON.parse(
      fs.readFileSync('./assets/products.json').toString(),
    ).products;

    this.icons = JSON.parse(
      fs.readFileSync('./assets/icons.json').toString(),
    ).icons;
  }

  getProducts(): FoodProduct[] {
    return this.products;
  }

  getProductIcons(): FoodProductIcon[] {
    return this.icons;
  }
}
