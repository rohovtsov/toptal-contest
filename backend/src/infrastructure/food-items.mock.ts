import { AccountsService } from './accounts.service';
import { FoodItemsService } from './food-items.service';
import { FoodProductsService } from './food-products.service';

export function mockFoodItemsFactory(
  accounts: AccountsService,
  products: FoodProductsService,
): FoodItemsService {
  const food = new FoodItemsService();
  mockFoodItems(accounts, food, products);
  return food;
}

function prob(prob: number): boolean {
  return Math.random() < prob / 100;
}

function mockFoodItems(
  accounts: AccountsService,
  foodItems: FoodItemsService,
  productsService: FoodProductsService,
) {
  const products = productsService.getProducts();
  const now = Date.now();
  const hour = 3600000;
  const day = 24 * hour;

  for (const account of accounts.getUserAccounts()) {
    const userId = account.id;
    const count = 150;

    for (let i = 0; i < count; i++) {
      const product = products[Math.floor(Math.random() * products.length)];
      const timeDelta = prob(25)
        ? Math.floor(Math.random() * 50 * day)
        : Math.floor(Math.random() * 7 * day);
      const time = now - timeDelta;

      foodItems.addFood(userId, {
        takenDate: new Date(time).toISOString(),
        productCalories: product.calories,
        productName: product.name,
        productPriceUSD: prob(50) ? product.priceUSD : null,
      });
    }
  }
}
