import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AccountsService } from '../../infrastructure';

@Injectable()
export class UserAuthGuard implements CanActivate {
  constructor(private accounts: AccountsService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = request.headers.authorization;

    return token && !!this.accounts.getAccountByToken(token, 'user');
  }
}
