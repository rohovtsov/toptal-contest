import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  Headers,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AccountsService, FoodItemsService } from '../infrastructure';
import { AbstractFoodController } from './abstract-food.controller';
import { AdminAuthGuard } from './guards/admin-auth.guard';

@Controller('admin')
export class AdminController extends AbstractFoodController {
  constructor(
    protected foodItems: FoodItemsService,
    protected accounts: AccountsService,
  ) {
    super(foodItems, accounts);
  }

  @Get('food')
  @UseGuards(AdminAuthGuard)
  @Header('content-type', 'application/json')
  getFood(@Query('fromTime') fromTime, @Query('toTime') toTime): string {
    return JSON.stringify(
      this.foodItems.getFood(null, fromTime ?? null, toTime ?? null),
    );
  }

  @Post('food')
  @UseGuards(AdminAuthGuard)
  @Header('content-type', 'application/json')
  addFood(@Body() body, @Query('userId') userId): void {
    const newFood = this.validateFoodPrototype(body);
    this.addFoodSafe(userId, newFood);
  }

  @Put('food/:id')
  @UseGuards(AdminAuthGuard)
  @Header('content-type', 'application/json')
  setFood(@Param('id') foodId, @Body() body): void {
    const food = this.food(foodId);
    const newFood = this.validateFoodPrototype(body);
    this.foodItems.updateFood(food, newFood);
  }

  @Delete('food/:id')
  @UseGuards(AdminAuthGuard)
  @Header('content-type', 'application/json')
  removeFood(@Param('id') foodId): void {
    const food = this.food(foodId);
    this.foodItems.removeFood(food);
  }

  @Get('details')
  @UseGuards(AdminAuthGuard)
  @Header('content-type', 'application/json')
  getDetails(@Headers() headers): string {
    const account = this.adminAccount(headers);
    return JSON.stringify(account);
  }

  @Get('users')
  @UseGuards(AdminAuthGuard)
  @Header('content-type', 'application/json')
  getUsers(): string {
    return JSON.stringify(this.accounts.getUserAccounts());
  }
}
