import {
  Controller,
  Delete,
  Get,
  Header,
  Post,
  Put,
  UseGuards,
  Headers,
  Param,
  Body,
  Query,
} from '@nestjs/common';
import { AccountsService, FoodItemsService } from '../infrastructure';
import { UserAuthGuard } from './guards/user-auth.guard';
import { AbstractFoodController } from './abstract-food.controller';

@Controller('user')
export class UserController extends AbstractFoodController {
  constructor(
    protected foodItems: FoodItemsService,
    protected accounts: AccountsService,
  ) {
    super(foodItems, accounts);
  }

  @Get('food')
  @UseGuards(UserAuthGuard)
  @Header('content-type', 'application/json')
  getFood(
    @Headers() headers,
    @Query('fromTime') fromTime,
    @Query('toTime') toTime,
  ): string {
    return JSON.stringify(
      this.foodItems.getFood(
        this.userAccount(headers).id,
        fromTime ?? null,
        toTime ?? null,
      ),
    );
  }

  @Post('food')
  @UseGuards(UserAuthGuard)
  @Header('content-type', 'application/json')
  addFood(@Headers() headers, @Body() body): void {
    const newFood = this.validateFoodPrototype(body);
    this.addFoodSafe(this.userAccount(headers).id, newFood);
  }

  @Put('food/:id')
  @UseGuards(UserAuthGuard)
  @Header('content-type', 'application/json')
  setFood(@Headers() headers, @Param('id') foodId, @Body() body): void {
    const food = this.userFood(headers, foodId);
    const newFood = this.validateFoodPrototype(body);
    this.foodItems.updateFood(food, newFood);
  }

  @Delete('food/:id')
  @UseGuards(UserAuthGuard)
  @Header('content-type', 'application/json')
  removeFood(@Headers() headers, @Param('id') foodId): void {
    const food = this.userFood(headers, foodId);
    this.foodItems.removeFood(food);
  }

  @Get('details')
  @UseGuards(UserAuthGuard)
  @Header('content-type', 'application/json')
  getDetails(@Headers() headers): string {
    const account = this.userAccount(headers);
    return JSON.stringify(account);
  }
}
