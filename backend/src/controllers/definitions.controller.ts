import { Controller, Get, Header } from '@nestjs/common';
import { FoodProductsService } from '../infrastructure';

@Controller('definitions')
export class DefinitionsController {
  constructor(private products: FoodProductsService) {}

  @Get('products')
  @Header('content-type', 'application/json')
  getProducts(): string {
    return JSON.stringify(this.products.getProducts());
  }

  @Get('productIcons')
  @Header('content-type', 'application/json')
  getIcons(): string {
    return JSON.stringify(this.products.getProductIcons());
  }
}
