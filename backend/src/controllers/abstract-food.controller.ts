import { HttpException, HttpStatus } from '@nestjs/common';
import { AccountsService, FoodItemsService } from '../infrastructure';
import {
  AccountDetails,
  FoodItem,
  FoodItemPrototype,
  UserAccountDetails,
} from '../entities';
import { FOOD_ITEM_PROTOTYPE_VALIDATION, validateObject } from '../utils';

export class AbstractFoodController {
  constructor(
    protected foodItems: FoodItemsService,
    protected accounts: AccountsService,
  ) {}

  protected validateFoodPrototype(
    rawFood: Record<string, any>,
  ): FoodItemPrototype {
    try {
      return validateObject<FoodItemPrototype>(
        rawFood,
        FOOD_ITEM_PROTOTYPE_VALIDATION,
      );
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  protected addFoodSafe(
    userIdRaw: string | number,
    newFood: FoodItemPrototype,
  ) {
    const userId = Number(userIdRaw);

    if (!this.accounts.hasUserAccount(userId)) {
      throw new HttpException(
        `Invalid userId=${userIdRaw}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    this.foodItems.addFood(Number(userId), newFood);
  }

  protected userFood(
    headers: Record<string, string>,
    foodId: string,
  ): FoodItem {
    return this.food(foodId, this.userAccount(headers).id);
  }

  protected food(foodId: string, userId: number | null = null): FoodItem {
    const food = this.foodItems.getFoodById(Number(foodId), userId);

    if (!food) {
      throw new HttpException(
        `Food not found, id=${foodId}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    return food;
  }

  protected userAccount(headers: Record<string, string>): UserAccountDetails {
    return this.accounts.getAccountByToken(headers.authorization, 'user');
  }

  protected adminAccount(headers: Record<string, string>): AccountDetails {
    return this.accounts.getAccountByToken(headers.authorization, 'admin');
  }
}
