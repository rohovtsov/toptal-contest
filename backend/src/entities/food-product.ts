export interface FoodProduct {
  name: string;
  priceUSD: number;
  calories: number;
}

export interface FoodProductIcon {
  name: string;
  icon: string;
}
