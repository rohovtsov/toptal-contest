export interface FoodItem {
  id: number;
  userId: number;
  takenDate: string;
  productName: string;
  productCalories: number;
  productPriceUSD: number | null;
}

export interface FoodItemPrototype {
  takenDate: string;
  productName: string;
  productCalories: number;
  productPriceUSD: number | null;
}
