export type AccountRole = 'user' | 'admin';

export interface AccountDetails {
  id: number;
  firstName: string;
  lastName: string;
  avatarUrl: string;
}

export interface AccountFullDetails extends AccountDetails {
  authToken: string;
  role: AccountRole;
}

export interface UserAccountDetails extends AccountDetails {
  dailyCaloriesLimit: number;
  monthlyBudgetLimitUSD: number;
  role: 'user';
}

export interface AdminAccountDetails extends AccountDetails {
  role: 'admin';
}
