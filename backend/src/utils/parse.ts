import * as fs from 'fs';

function parseIconsTxt(): void {
  const src = fs.readFileSync('./assets/icons.txt').toString();
  const entries = src
    .split('\n')
    .map((entry) => entry.trim())
    .filter(Boolean);

  const res = entries.map((entry) => {
    const icon = entry.slice(0, 2);
    const name = entry.slice(2, entry.length).trim();

    return { icon, name };
  });

  fs.writeFileSync(
    './assets/icons.json',
    JSON.stringify({ icons: res }, null, 2),
  );
}

function parseProducts(): void {
  const icons = JSON.parse(
    fs.readFileSync('./assets/icons.json').toString(),
  ).icons;

  const res = icons.map((icon) => {
    const calories = Math.floor(Math.random() * 240 + 10);
    const priceUSD = Math.floor(Math.random() * 5000 + 50) / 100;

    return { name: icon.name, calories, priceUSD };
  });

  fs.writeFileSync(
    './assets/products.json',
    JSON.stringify({ products: res }, null, 2),
  );
}

//parseIconsTxt();
//parseProducts();
