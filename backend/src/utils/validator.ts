export interface ValidatorProperty {
  name: string;
  type: string;
  required?: boolean;
  validationFn?: (value: any) => boolean;
}

export const FOOD_ITEM_PROTOTYPE_VALIDATION: ValidatorProperty[] = [
  {
    name: 'takenDate',
    required: true,
    type: 'date',
  },
  {
    name: 'productName',
    required: true,
    type: 'string',
  },
  {
    name: 'productCalories',
    required: true,
    type: 'number',
    validationFn: (val: number) => val > 0,
  },
  {
    name: 'productPriceUSD',
    type: 'number',
    validationFn: (val: number) => val === null || val > 0,
  },
];

export function validateObject<T>(
  rawObj: Record<string, any>,
  props: ValidatorProperty[],
): T {
  const obj = {} as T;

  for (const p of props) {
    const val = rawObj[p.name];

    if ((val === null || val === undefined) && p.required) {
      throw new Error(`Missing property "${p.name}"`);
    } else if (val === undefined) {
      continue;
    }

    if (
      (p.type === 'date' && new Date(val).toISOString() !== val) ||
      (p.type !== 'date' && val !== null && typeof val !== p.type)
    ) {
      throw new Error(`Incorrect type for "${p.name}", needed "${p.type}"`);
    }

    if (p.validationFn && !p.validationFn(val)) {
      throw new Error(`Incorrect value "${p.name}"`);
    }

    obj[p.name] = val;
  }

  return obj;
}
