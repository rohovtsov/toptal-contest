import { Module } from '@nestjs/common';
import {
  AccountsService,
  FoodItemsService,
  FoodProductsService,
  mockFoodItemsFactory,
} from './infrastructure';
import {
  AdminAuthGuard,
  AdminController,
  DefinitionsController,
  UserAuthGuard,
  UserController,
} from './controllers';

@Module({
  imports: [],
  controllers: [UserController, AdminController, DefinitionsController],
  providers: [
    FoodProductsService,
    UserAuthGuard,
    AdminAuthGuard,
    AccountsService,
    {
      provide: FoodItemsService,
      useFactory: mockFoodItemsFactory,
      inject: [AccountsService, FoodProductsService],
    },
  ],
})
export class AppModule {}
