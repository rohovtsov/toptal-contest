**Install dependencies**

1) cd backend && npm install
2) cd frontend && npm install

---

**Start app**

1) npm run startBackend
2) npm run startFrontend

---

**See available auth tokens and edit users**
/backend/assets/accounts.json
